package de.hhu.propra.examples.wurmclass;

public class Waffe {
  private int munition;
  private static final int schaden = 100;

  public Waffe(int munition) {
    this.munition = munition;
  }

  public int feuer() {
    if (munition > 0) {
      munition--;
      return schaden;
    } else {
      return 0;
    }
  }
}
