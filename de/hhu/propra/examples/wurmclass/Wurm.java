package de.hhu.propra.examples.wurmclass;

public class Wurm {
  public int leben;
  private int team;

  private Waffe waffe;

  public Wurm(Waffe waffe, int leben, int team) {
    this.waffe = waffe;
    this.leben = leben;
    this.team = team;
  }

  public int waffeAbfeuern() {
    return waffe.feuer();
  }

  public void schadenErleiden(int schaden) {
    leben = Math.max(0, leben - schaden);
  }
}
