package de.hhu.propra.examples.wurmclass;

public class Spiel {
  public static void main(String[] args) {
    Waffe waffe1 = new Waffe(25);
    Waffe waffe2 = new Waffe(7);
    Wurm wurm1 = new Wurm(waffe1, 100, 1);
    Wurm wurm2 = new Wurm(waffe2, 100, 2);

    wurm1.schadenErleiden(wurm2.waffeAbfeuern());

    System.out.println(wurm1.leben);
  }
}
