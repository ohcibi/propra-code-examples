Code Examples
=============

Klonen
------

```sh
git clone https://ohcibi@bitbucket.org/ohcibi/propra-code-examples.git
```

Dies erstellt einen Ordner `propra-code-examples` im aktuellen Verzeichnis. Wenn es nicht explizit
anders erwähnt wird, sind alle Befehle aus diesem Ordner heraus auszuführen.

```sh
cd propra-code-examples
```

Kompilieren
-----------

```sh
javac de/hhu/propra/examples/<example-package>/*.java
```

Ausführen
---------

```sh
java de.hhu.propra.examples.<example-package>.HauptKlasse
```
